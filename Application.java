class Application{
  public static void main(String[]args){
    Student student1 = new Student();
    Student student2 = new Student();
    
    student1.grade = 70;
    student1.name = "Kenny";
    student1.hobby = "drawing";
    
    student2.grade =50;
    student2.name = "Alexa";
    student2.hobby = "reading";
    
    System.out.println(student1.grade);
    System.out.println(student1.name);
    System.out.println(student1.hobby);
    System.out.println(student2.grade);
    System.out.println(student2.name);
    System.out.println(student2.hobby);
    
    student1.gradeRating();
    student1.nameMessage();
    student2.gradeRating();
    student2.nameMessage(); 
	
	Student[] section3 = new Student[3];
	section3[0] = student1;
	section3[1] = student2;
	
	System.out.println("");
	System.out.println(section3[0].grade);
	System.out.println("");
	section3[2] = new Student();
	section3[2].grade = 100;
	section3[2].name = "luffy";
	section3[2].hobby = "becoming the king of pirates";
	System.out.println(section3[2].grade);
	System.out.println(section3[2].name);
	System.out.println(section3[2].hobby);
  }
  
}